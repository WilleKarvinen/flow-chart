const randomize = () => {
    let num1 = parseInt(document.querySelector("#firstNum").value)
    let num2 = parseInt(document.querySelector("#secNum").value)
    let fin = document.querySelector("#final")
    let err = document.querySelector("#error")
    let mil = 1000000

    if (num1) {
        if (num1 >= 0 && num1 <= mil) {
            if (num2) {
                // Both numbers used
                if (num2 >= 0 && num2 <= mil && num2 > num1) {
                    fin.innerHTML = Math.floor(Math.random() * (Math.floor(num2) - Math.ceil(num1) + 1)) + Math.ceil(num1)
                    err.style.visibility = "hidden"
                } else {
                    err.innerHTML = "Incorrect 2nd number input"
                    err.style.visibility = "visible"
                }
            }
            // Only first number used
            else {
                fin.innerHTML = Math.floor(Math.random() * (Math.floor(mil) - Math.ceil(num1) + 1)) + Math.ceil(num1)
                err.style.visibility = "hidden"
            }
        } else {
            err.innerHTML = "Incorrect 1st number input"
            err.style.visibility = "visible"
        }
    }
    // Only second number used
    else if (num2) {
        if (num2 >= 0 && num2 <= mil) {
            fin.innerHTML = Math.floor(Math.random() * (Math.floor(num2) + 1))
            err.style.visibility = "hidden"
        } else {
            err.innerHTML = "Incorrect 2nd number input"
            err.style.visibility = "visible"
        }
    }
    // No numbers used
    else {
        fin.innerHTML = Math.round(Math.random() * mil)
        err.style.visibility = "hidden"
    }
}